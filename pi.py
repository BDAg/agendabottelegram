import telebot
import datetime
import time
from queue import Queue

TOKEN = '6983343144:AAE0Y-fBY6hGZa_FuXaDXxbqGbVFWFA4zS4'

bot = telebot.TeleBot(TOKEN)

agenda = []
mensagens_pendentes = Queue()  

@bot.message_handler(commands=['start'])
def start(message):
    bot.reply_to(message, "Olá. Tudo bem? Bem-vindo ao agendabot! Use o formato 'dd/mm/yyyy hh:mm Descrição' para adicionar um compromisso.")

@bot.message_handler(func=lambda message: True)
def adicionar_compromisso(message):
    try:
        input_parts = message.text.split(' ', 2)  
        if len(input_parts) != 3:
            raise ValueError

        data = datetime.datetime.strptime(input_parts[0], '%d/%m/%Y')
        horario = datetime.datetime.strptime(input_parts[1], '%H:%M')

        descricao = input_parts[2]

        compromisso = {'data': data, 'horario': horario, 'descricao': descricao}
        agenda.append(compromisso)

        bot.reply_to(message, f'Compromisso adicionado: {data.strftime("%d/%m/%Y")} às {horario.strftime("%H:%M")} - {descricao}')

    except (ValueError, IndexError):
        bot.reply_to(message, 'Formato inválido. Use o formato "dd/mm/yyyy hh:mm Descrição".')

def verificar_compromissos():
    global mensagens_pendentes

    while True:
        agora = datetime.datetime.now()

        compromissos_proximos = []
        for compromisso in agenda:
            data_compromisso = compromisso['data']
            horario_compromisso = compromisso['horario']

            diff = datetime.datetime.combine(data_compromisso, horario_compromisso.time()) - agora

            if 0 < diff.total_seconds() <= 900:
                compromissos_proximos.append(compromisso)

        if compromissos_proximos:
            for compromisso in compromissos_proximos:
                descricao = compromisso['descricao']
                mensagens_pendentes.put(f'Tem um compromisso perto, fique alerta! {descricao}')  

        time.sleep(60) 

def enviar_mensagens():
    global mensagens_pendentes

    while True:
        if not mensagens_pendentes.empty():  
            mensagem = mensagens_pendentes.get()  
           
           
            chat_id = "6410093533" 
            bot.send_message(chat_id, mensagem)

        time.sleep(1)

if __name__ == '__main__':
    import threading

    thread_verificar = threading.Thread(target=verificar_compromissos)
    thread_verificar.daemon = True
    thread_verificar.start()

    thread_enviar = threading.Thread(target=enviar_mensagens)
    thread_enviar.daemon = True
    thread_enviar.start()

    bot.polling()
